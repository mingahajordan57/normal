<?php

require_once 'database.php';
require_once 'Course.php';

class Student {
    private $id;
    private $name;
    private $age;
    private $level;
    private $courseId;
    private $pdo;
  
    public function __construct($id, $name, $age, $level, $pdo) {
      $this->id = $id;
      $this->name = $name;
      $this->age = $age;
      $this->level = $level;
      $this->courseId = null;
      $this->pdo = $pdo;
    }
  
    public function getId() {
      return $this->id;
    }
  
    public function getName() {
      return $this->name;
    }
  
    public function getAge() {
      return $this->age;
    }
  
    public function getLevel() {
      return $this->level;
    }
  
    public function setName($name) {
      $this->name = $name;
    }
  
    public function setAge($age) {
      $this->age = $age;
    }
  
    public function setLevel($level):void { $this->level = $level;}
  
    public function setCourse($courseId) {
      $this->courseId = $courseId;
    }
    public function getCourse() {
        if ($this->courseId) {
          $query = "SELECT * FROM courses WHERE id = ?";
          $stmt = $this->pdo->prepare($query);
          $stmt->execute([$this->courseId]);
          return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        return null;
      }
  
    public function save() {
      $query = 'INSERT INTO students (id, name, age, level, course_id) VALUES (?, ?, ?, ?, ?)';
      $stmt = $this->pdo->prepare($query);
      $stmt->execute([$this->id, $this->name, $this->age, $this->level, $this->courseId]);
    }
  
    public function update() {
      $query = 'UPDATE students SET name = ?, age = ?, level = ?, course_id = ? WHERE id = ?';
      $stmt = $this->pdo->prepare($query);
      $stmt->execute([$this->name, $this->age, $this->level, $this->courseId, $this->id]);
    }
  
    public function delete() {
      $query = 'DELETE FROM students WHERE id = ?';
      $stmt = $this->pdo->prepare($query);
      $stmt->execute([$this->id]);
    }

    public function searchContact($search) {
      if (empty($search)) { echo('search doit être renseigné');}
      if (!is_string($search)) { echo('search doit être une chaine de caractères');}
      $req = "SELECT * from students where name like '%" . $search . "%' or id like '%" . $search . "%'";

      $res = $this->pdo->query($req);

      $row = $res->fetchAll();

      if ($res) { return $row; }
    
  }
  }