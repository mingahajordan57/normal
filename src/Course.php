<?php

require_once 'database.php';
require_once 'Student.php';
// require_once 'Course.php';

class Course {
  private $id;
  private $name;
  private $teacher;
  private $pdo;

  public function __construct($id, $name, $teacher, $pdo) {
    $this->id = $id;
    $this->name = $name;
    $this->teacher = $teacher;
    $this->pdo = $pdo;
  }

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return $this->name;
  }

  public function getTeacher() {
    return $this->teacher;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function setTeacher($teacher) {
    $this->teacher = $teacher;
  }
//   public function getStudents() {
//     $query = "SELECT * FROM students WHERE course_id = ?";
//     $stmt = $this->pdo->prepare($query);
//     $stmt->execute([$this->id]);
//     return $stmt->fetchAll(PDO::FETCH_ASSOC);
//   }

  public function save() {
    $query = 'INSERT INTO courses (id, name, teacher) VALUES (?, ?, ?)';
    $stmt = $this->pdo->prepare($query);
    $stmt->execute([$this->id, $this->name, $this->teacher]);
  }

  public function update() {
    $query = 'UPDATE courses SET name = ?, teacher = ? WHERE id = ?';
    $stmt = $this->pdo->prepare($query);
    $stmt->execute([$this->name, $this->teacher, $this->id]);
  }

  public function delete() {
    $query = 'DELETE FROM courses WHERE id = ?';
    $stmt = $this->pdo->prepare($query);
    $stmt->execute([$this->id]);
  }
}
