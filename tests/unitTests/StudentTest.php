<?php

use PHPUnit\Framework\TestCase;
require_once __DIR__.'/../../src/database.php';
require_once __DIR__.'/../../src/Student.php';
require_once __DIR__.'/../../src/Course.php';

/**
 * * @covers invalidInputException
 * @covers \ContactService
 *
 * @internal
 */
class StudentTest extends TestCase {
  private $pdo;

  protected function setUp(): void {
    $this->pdo = new PDO('sqlite::memory:');
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->pdo->exec("CREATE TABLE students (
      id INTEGER PRIMARY KEY,
      name TEXT,
      age INTEGER,
      level TEXT,
      course_id INTEGER
    )");
  }

  protected function tearDown(): void {
    $this->pdo->exec("DROP TABLE students");
  }

  public function testSaveStudent(): void {
    $student = new Student(1, 'John Doe', 25, 'Master 2', $this->pdo);
    $student->save();

    $stmt = $this->pdo->query("SELECT COUNT(*) FROM students");
    $count = $stmt->fetchColumn();
    
    $this->assertEquals(1, $count);
  }

  public function testUpdateStudent(): void {
    $student = new Student(1, 'John Doe', 25, 'Master 2', $this->pdo);
    $student->save();

    $student->setName('Jane Doe');
    $student->setAge(26);
    $student->setLevel('Master 3');
    $student->update();

    $stmt = $this->pdo->prepare("SELECT name, age, level FROM students WHERE id = ?");
    $stmt->execute([$student->getId()]);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    
    $expectedData = [
      'name' => 'Jane Doe',
      'age' => 26,
      'level' => 'Master 3'
    ];

    $this->assertEquals($expectedData, $data);
  }

  public function testDeleteStudent(): void {
    $student = new Student(1, 'John Doe', 25, 'Master 2', $this->pdo);
    $student->save();

    $student->delete();

    $stmt = $this->pdo->query("SELECT COUNT(*) FROM students");
    $count = $stmt->fetchColumn();
    
    $this->assertEquals(0, $count);
  }

  // Add more tests for other methods in the Student class
}
