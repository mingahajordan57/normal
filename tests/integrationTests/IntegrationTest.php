<?php

use PHPUnit\Framework\TestCase;
require_once __DIR__.'/../../src/database.php';
require_once __DIR__.'/../../src/Student.php';
require_once __DIR__.'/../../src/Course.php';

/**
 * * @covers invalidInputException
 * @covers \ContactService
 *
 * @internal
 */

class IntegrationTest extends TestCase {
  private $pdo;

  protected function setUp(): void {
    $this->pdo = new PDO('sqlite::memory:');
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->pdo->exec("CREATE TABLE students (
      id INTEGER PRIMARY KEY,
      name TEXT,
      age INTEGER,
      level TEXT,
      course_id INTEGER
    )");
    $this->pdo->exec("CREATE TABLE courses (
      id INTEGER PRIMARY KEY,
      name TEXT,
      teacher TEXT
    )");
  }

  protected function tearDown(): void {
    $this->pdo->exec("DROP TABLE students");
    $this->pdo->exec("DROP TABLE courses");
  }

  public function testStudentCourseIntegration(): void {
    // Create a course
    $course = new Course(1, 'Mathematics', 'Prof. Johnson', $this->pdo);
    $course->save();

    // Create a student
    $student = new Student(1, 'John Doe', 25, 'Master 2', $this->pdo);
    $student->save();

    // Assign the course to the student
    $student->setCourse($course->getId());
    $student->update();

    // Retrieve the student with the assigned course
    $stmt = $this->pdo->prepare("SELECT students.id, students.name, students.age, students.level, courses.name AS course_name, courses.teacher
                                 FROM students
                                 LEFT JOIN courses ON students.course_id = courses.id
                                 WHERE students.id = ?");
    $stmt->execute([$student->getId()]);
    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    $expectedData = [
      'id' => 1,
      'name' => 'John Doe',
      'age' => 25,
      'level' => 'Master 2',
      'course_name' => 'Mathematics',
      'teacher' => 'Prof. Johnson'
    ];

    $this->assertEquals($expectedData, $data);
  }
  public function testStudentCourseIntegrations(): void {
    // Create a course
    $course = new Course(1, 'Mathematics', 'Prof. Johnson', $this->pdo);
    $course->save();
  
    // Create a student
    $student = new Student(1, 'John Doe', 25, 'Master 2', $this->pdo);
    $student->setCourse($course->getId());
    $student->save();
  
    // Retrieve the student with the associated course
    $studentData = $student->getCourse();
  
    $expectedData = [
      'id' => $course->getId(),
      'name' => $course->getName(),
      'teacher' => $course->getTeacher()
    ];
  
    $this->assertEquals($expectedData, $studentData);
  }
  
}

?>
