<?php
use PHPUnit\Framework\TestCase;
require_once __DIR__.'/../../src/database.php';
require_once __DIR__.'/../../src/Student.php';
require_once __DIR__.'/../../src/Course.php';
/**
 * * @covers invalidInputException
 * @covers \ContactService
 *
 * @internal
 */


 
 class CourseTest extends TestCase {
   private $pdo;
 
   protected function setUp(): void {
     $this->pdo = new PDO('sqlite::memory:');
     $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $this->pdo->exec("CREATE TABLE courses (
       id INTEGER PRIMARY KEY,
       name TEXT,
       teacher TEXT
     )");
   }
 
   protected function tearDown(): void {
     $this->pdo->exec("DROP TABLE courses");
   }
 
   public function testSaveCourse(): void {
     $course = new Course(1, 'Mathematics', 'Prof. Johnson', $this->pdo);
     $course->save();
 
     $stmt = $this->pdo->query("SELECT COUNT(*) FROM courses");
     $count = $stmt->fetchColumn();
     
     $this->assertEquals(1, $count);
   }
 
   public function testUpdateCourse(): void {
     $course = new Course(1, 'Mathematics', 'Prof. Johnson', $this->pdo);
     $course->save();
 
     $course->setName('Physics');
     $course->setTeacher('Prof. Smith');
     $course->update();
 
     $stmt = $this->pdo->prepare("SELECT name, teacher FROM courses WHERE id = ?");
     $stmt->execute([$course->getId()]);
     $data = $stmt->fetch(PDO::FETCH_ASSOC);
     
     $expectedData = [
       'name' => 'Physics',
       'teacher' => 'Prof. Smith'
     ];
 
     $this->assertEquals($expectedData, $data);
   }
 
   public function testDeleteCourse(): void {
     $course = new Course(1, 'Mathematics', 'Prof. Johnson', $this->pdo);
     $course->save();
 
     $course->delete();
 
     $stmt = $this->pdo->query("SELECT COUNT(*) FROM courses");
     $count = $stmt->fetchColumn();
     
     $this->assertEquals(0, $count);
   }
 
   // Add more tests for other methods in the Course class
 }
 