<?php

try {
  $pdo = new PDO('sqlite:database.db');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $pdo->exec("CREATE TABLE IF NOT EXISTS students (
    id INTEGER PRIMARY KEY,
    name TEXT,
    age INTEGER,
    level TEXT
  )");

  $pdo->exec("CREATE TABLE IF NOT EXISTS courses (
    id INTEGER PRIMARY KEY,
    name TEXT,
    teacher TEXT
  )");
} catch (PDOException $e) {
  echo "Erreur de connexion à la base de données : " . $e->getMessage();
}

?>
